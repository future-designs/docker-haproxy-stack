#!/usr/bin/env bash

configHaproxyConfig="/opt/mydocker/config/haproxy/haproxy.cfg"
containerHaproxyConfig="/etc/haproxy/haproxy.cfg"
oldContainerHaproxyConfig="/etc/haproxy/haproxy.cfg.old"

delete_haproxy_config() {
    echo -e "\e[32mSearching for existing cfg file in /etc/haproxy/ ...\e[0m"

    if [ -f $containerHaproxyConfig ]
    then
        echo -e "\e[31mCfg-file found, try to rename ...\e[0m"
        mv -f $containerHaproxyConfig $oldContainerHaproxyConfig

        # check success
        if [ ! -f $containerHaproxyConfig ] && [ -f $oldContainerHaproxyConfig ]
        then
            echo -e "\e[32mOld cfg-file renamed.\e[0m"
        else
            echo -e "\e[31mRename of old cfg-file failed.\e[31m"
        fi
    else
        echo -e "\e[32mNo cfg-file found.\e[0m"
    fi
}

check_config_copy() {
    echo -e "\e[32mChecking copied file ...\e[0m"
    if [ -f $containerHaproxyConfig ]
    then
        echo -e "\e[32mCopy successful.\e[0m"
    else
        echo -e "\e[31mNo file found.\e[31m"
    fi
}

# installation
yum -y install haproxy

# clear old haproxy config
delete_haproxy_config

# copy new config
cp $configHaproxyConfig $containerHaproxyConfig

#check success
check_config_copy

# start and autostart
systemctl start haproxy
systemctl enable haproxy

# network
#yum install bridge-utils

# firewall

